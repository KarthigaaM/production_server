#!/usr/bin/expect
spawn ssh-copy-id rig@52.226.18.21
expect {
    {continue connecting (yes/no)?} {
        send "yes\r"
        exp_continue
    }
"*password*" {
        send "digitalrig@123\r"
    }
}
spawn ssh rig@52.226.18.21
expect "password"
send "digitalrig@123\r"
interact
